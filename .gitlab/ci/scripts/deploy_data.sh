#!/bin/bash
set -e
for file in packaged_data/*; do
    gzip ${file}
    mv ${file}.gz ${file}
done
rsync -av --no-links packaged_data/ kitware@web.kitware.com:/data/
