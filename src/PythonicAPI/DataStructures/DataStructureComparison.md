### Description

In Python use the 'u' key to go up a level and the 'd' key to go down a level.
 
!!! info
    Using the 'p' key in Python will generate the warning **no current renderer on the interactor style**.
