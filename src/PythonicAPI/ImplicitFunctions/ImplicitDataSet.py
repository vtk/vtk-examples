#!/usr/bin/env python3

from vtkmodules.vtkCommonDataModel import vtkImplicitDataSet
from vtkmodules.vtkImagingCore import vtkRTAnalyticSource


def main():
    wavelet_source = vtkRTAnalyticSource()

    implicit_wavelet = vtkImplicitDataSet(data_set=wavelet_source.update().output)

    x = (0.5, 0, 0)
    print(f'The value should roughly be 258.658:\n'
          f'x: {implicit_wavelet.EvaluateFunction(x):0.3f}')


if __name__ == '__main__':
    main()
