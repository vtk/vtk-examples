#!/usr/bin/env python3

from vtkmodules.vtkCommonCore import (
    vtkFileOutputWindow,
    vtkOutputWindow
)
from vtkmodules.vtkIOXML import vtkXMLPolyDataReader


def main():
    file_output_window = vtkFileOutputWindow(file_name='FileOutputWindow.txt')

    # Note that the SetInstance function is a static member of vtkOutputWindow.
    vtkOutputWindow.SetInstance(file_output_window)

    # This causes an error intentionally (file name not specified) - this error
    # will be written to the file output.txt
    reader = vtkXMLPolyDataReader()
    reader.Update()


if __name__ == '__main__':
    main()
