### Description

This example demonstrates the use of vtkFreeTypeTools to populate an image with multiple strings converted into images. The final image is created using vtkImageBlend to blend each string image into the final image. If a string image does not fit in the final image or overlaps with an image in final, that sting image is skipped.

This example differs from the C++ example in that vtkImageIterator cannot be used since it is designed for use for C++. Accordingly, we implement the Separating Axis Theorem to determine if the rectangles corresponding to the text overlap.

See: [How to check intersection between 2 rotated rectangles?](https://stackoverflow.com/questions/10962379/how-to-check-intersection-between-2-rotated-rectangles)

Tfe final result differs slightly from the C++ example in that only 15 and not 19 images are rendered.
