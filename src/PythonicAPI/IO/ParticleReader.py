#!/usr/bin/env python3

from pathlib import Path

# noinspection PyUnresolvedReferences
import vtkmodules.vtkRenderingOpenGL2
from vtkmodules.vtkCommonColor import vtkNamedColors
from vtkmodules.vtkIOGeometry import vtkParticleReader
from vtkmodules.vtkRenderingCore import (
    vtkActor,
    vtkPolyDataMapper,
    vtkRenderer,
    vtkRenderWindow,
    vtkRenderWindowInteractor
)


def get_program_parameters():
    import argparse
    description = 'ParticleReader.'
    epilogue = '''
    '''
    parser = argparse.ArgumentParser(description=description, epilog=epilogue,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('filename', help='Particles.raw')
    args = parser.parse_args()
    return args.filename


def main():
    colors = vtkNamedColors()

    ifn = get_program_parameters()

    # Check that the file exists.
    ifp = Path(ifn)
    if not ifp.is_file():
        print(f'Nonexistent source: {ifp}')
        return

    # Particles.raw supplied by VTK is big endian encoded.
    # Read the file.
    reader = vtkParticleReader(file_name=ifp)

    # If nothing gets displayed or totally wrong, swap the endianness.
    reader.SetDataByteOrderToBigEndian()
    reader.update()

    # Visualize
    mapper = vtkPolyDataMapper(scalar_range=(4, 9))
    reader >> mapper
    print(f'Number of pieces: {mapper.GetNumberOfPieces()}')

    actor = vtkActor(mapper=mapper)
    actor.property.point_size = 4

    renderer = vtkRenderer()
    render_window = vtkRenderWindow()
    render_window.AddRenderer(renderer)
    render_window.window_name = 'ParticleReader'

    render_window_interactor = vtkRenderWindowInteractor()
    render_window_interactor.render_window = render_window

    renderer.AddActor(actor)
    renderer.background = colors.GetColor3d('DarkSlateGray')

    render_window.Render()
    render_window_interactor.Start()


if __name__ == '__main__':
    main()
