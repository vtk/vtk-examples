### Description

Animate a sphere by opening it to an angle of 90°. The edge of the opened sphere is colored red.

This example was inspired by this discussion: [How to get a Python version of the User Guide ?vtkAnimationScene? example running?](https://discourse.vtk.org/t/how-to-get-a-python-version-of-the-user-guide-vtkanimationscene-example-running/11440/).
