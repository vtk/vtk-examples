### Description

Unlike in C++ where vtkImageIterator is an efficient way to access the regions of a vtkImageData. We have to use indexing instead.

The differences from the C++ example are:
- We cannot use pointers in Python so we use `SetScalarComponentFromDoubl(...)` and `GetScalarComponentFromDouble(...)` instead of `GetScalarPointer(...)`.
- We cannot use vtkImageIterator as it is not wrapped in Python.
So we use indexing instead.
