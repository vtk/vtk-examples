### Description

This example shows how to draw the outline of the dataset.

!!! seealso
    [BoundingBox](../../../PythonicAPI/Utilities/BoundingBox) and [CubeAxesActor2D](../../../PythonicAPI/Visualization/CubeAxesActor2D).
