#!/usr/bin/env python3

# noinspection PyUnresolvedReferences
import vtkmodules.vtkRenderingOpenGL2
from vtkmodules.vtkFiltersCore import vtkPassThrough
from vtkmodules.vtkFiltersSources import vtkSphereSource


def main():
    # Create a sphere
    sphere_source = vtkSphereSource()
    sphere_source.update()

    print(f'Points before: {sphere_source.output.number_of_points}')

    pass_through = vtkPassThrough()
    sphere_source >> pass_through
    pass_through.update()

    output = pass_through.GetOutput()

    print(f'Points after: {output.number_of_points}')


if __name__ == '__main__':
    main()
