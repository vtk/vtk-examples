### Description

The first line of the output is now `"column-0","column-1","column-2"`.

The first line is the names of the column arrays in the table.

I added them to the example because the example was crashing on Windows builds, where streaming a NULL char* is no bueno. (David Cole)
