### Description

Generate image data containing spatio-temporal harmonic data.

This source allows you to specify the uniform grid extent. It also lets you choose the harmonics you want.

The source has an embedded filter allowing you to add mutiple harmonics defined by their amplitude, temporal frequency, wave vector, and phase. The sum of these will be computed using the sine function for each point. Note that there is no cosine in this function. If no harmonic is specified, values will be null.

The source can also generate time steps by specifying time values.
