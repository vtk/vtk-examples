#!/usr/bin/env python3

# noinspection PyUnresolvedReferences
import vtkmodules.vtkInteractionStyle
# noinspection PyUnresolvedReferences
import vtkmodules.vtkRenderingOpenGL2
# noinspection PyUnresolvedReferences
import vtkmodules.vtkRenderingUI
from vtkmodules.vtkCommonColor import vtkNamedColors
from vtkmodules.vtkFiltersSources import vtkSpatioTemporalHarmonicsSource
from vtkmodules.vtkInteractionWidgets import (
    vtkCameraOrientationWidget
)
from vtkmodules.vtkRenderingCore import (
    vtkActor,
    vtkDataSetMapper,
    vtkRenderer,
    vtkRenderWindow,
    vtkRenderWindowInteractor
)

MAX_EXTENT = 10


def main():
    colors = vtkNamedColors()

    # Create the source
    extent = (-MAX_EXTENT, MAX_EXTENT) * 3
    source = vtkSpatioTemporalHarmonicsSource(whole_extent=extent)

    source.ClearHarmonics()
    source.AddHarmonic(1.0, 1.0, 1.0, 0.0, 0.0, 0.0)
    source.AddHarmonic(2.0, 1.0, 0.0, 1.0, 0.0, 0.0)
    source.AddHarmonic(4.0, 1.0, 0.0, 0.0, 1.0, 0.0)

    source.ClearTimeStepValues()
    source.AddTimeStepValue(0.0)
    source.AddTimeStepValue(1.0)
    source.AddTimeStepValue(2.0)

    # Create the mapper and actor.
    mapper = vtkDataSetMapper(scalar_range=(-6.0, 6.0))
    source >> mapper
    actor = vtkActor(mapper=mapper)

    # Create a renderer, render window, and interactor.
    ren = vtkRenderer(background=colors.GetColor3d('Gray'))
    ren_win = vtkRenderWindow(size=(600, 600), window_name='SpatioTemporalHarmonicsSource')
    ren_win.AddRenderer(ren)
    iren = vtkRenderWindowInteractor()
    iren.render_window = ren_win
    # Since we import vtkmodules.vtkInteractionStyle we can do this
    # because vtkInteractorStyleSwitch is automatically imported:
    iren.interactor_style.SetCurrentStyleToTrackballCamera()

    ren.ResetCamera()
    ren.active_camera.position = (50.0, 40.0, 30.0)
    ren.active_camera.focal_point = (0.0, 0.0, 0.0)
    ren.ResetCameraClippingRange()

    # Add the actor, render and interact.
    ren.AddActor(actor)
    ren_win.Render()

    cow = vtkCameraOrientationWidget()
    cow.SetParentRenderer(ren)

    # Enable the widget.
    cow.On()
    iren.Start()


if __name__ == '__main__':
    main()
