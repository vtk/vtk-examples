#!/usr/bin/python3

# noinspection PyUnresolvedReferences
import vtkmodules.vtkInteractionStyle
# noinspection PyUnresolvedReferences
import vtkmodules.vtkRenderingOpenGL2
from vtkmodules.vtkCommonColor import vtkNamedColors
from vtkmodules.vtkInteractionWidgets import (
    vtkAngleRepresentation2D,
    vtkAngleWidget
)
from vtkmodules.vtkRenderingCore import (
    vtkRenderWindow,
    vtkRenderWindowInteractor,
    vtkRenderer
)


def main():
    colors = vtkNamedColors()

    # Create the Renderer, RenderWindow and RenderWindowInteractor.
    ren = vtkRenderer(background=colors.GetColor3d('MidnightBlue'))
    ren_win = vtkRenderWindow(window_name='AngleWidget2D')
    ren_win.AddRenderer(ren)
    iren = vtkRenderWindowInteractor()
    iren.render_window = ren_win

    pos1 = [50.0, 200.0, 0.0]
    pos2 = [200.0, 20.0, 0.0]
    center = [100.0, 100.0, 0.0]

    rep = vtkAngleRepresentation2D()

    angle_widget = vtkAngleWidget(representation=rep, interactor=iren)
    angle_widget.CreateDefaultRepresentation()

    ren_win.Render()

    angle_widget.On()

    rep.point1_display_position = pos1
    rep.point2_display_position = pos2
    rep.center_display_position = center
    rep.ray1_visibility = True
    rep.ray2_visibility = True
    rep.arc_visibility = True

    iren.Initialize()
    iren.Start()


if __name__ == '__main__':
    main()
