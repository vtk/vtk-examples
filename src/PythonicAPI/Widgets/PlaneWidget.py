#!/usr/bin/env python3

# noinspection PyUnresolvedReferences
import vtkmodules.vtkInteractionStyle
# noinspection PyUnresolvedReferences
import vtkmodules.vtkRenderingOpenGL2
# noinspection PyUnresolvedReferences
import vtkmodules.vtkRenderingUI
from vtkmodules.vtkCommonColor import vtkNamedColors
from vtkmodules.vtkInteractionWidgets import (
    vtkPlaneWidget
)
from vtkmodules.vtkRenderingCore import (
    vtkRenderWindow,
    vtkRenderWindowInteractor,
    vtkRenderer
)


def main():
    colors = vtkNamedColors()

    renderer = vtkRenderer(background=colors.GetColor3d('SlateGray'))
    render_window = vtkRenderWindow(window_name='PlaneWidget')
    render_window.AddRenderer(renderer)
    render_window_interactor = vtkRenderWindowInteractor()
    render_window_interactor.render_window = render_window

    plane_widget = vtkPlaneWidget()
    plane_widget.interactor = render_window_interactor

    plane_widget.On()

    render_window_interactor.Initialize()

    renderer.active_camera.Roll(30)
    renderer.active_camera.Pitch(45)
    renderer.ResetCameraClippingRange()

    renderer.ResetCamera()
    render_window.Render()
    render_window_interactor.Start()


if __name__ == '__main__':
    main()
