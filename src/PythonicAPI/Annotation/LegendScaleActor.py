#!/usr/bin/env python3

# noinspection PyUnresolvedReferences
import vtkmodules.vtkInteractionStyle
# noinspection PyUnresolvedReferences
import vtkmodules.vtkRenderingOpenGL2
from vtkmodules.vtkCommonColor import vtkNamedColors
from vtkmodules.vtkCommonComputationalGeometry import vtkParametricEnneper
from vtkmodules.vtkFiltersSources import vtkParametricFunctionSource
from vtkmodules.vtkRenderingAnnotation import vtkLegendScaleActor
from vtkmodules.vtkRenderingCore import (
    vtkActor,
    vtkPolyDataMapper,
    vtkRenderer,
    vtkRenderWindow,
    vtkRenderWindowInteractor
)


def main():
    colors = vtkNamedColors()

    # The source will be a parametric function.
    src = vtkParametricEnneper()
    fn_src = vtkParametricFunctionSource()
    fn_src.SetParametricFunction(src)

    # Create a mapper and actor.
    mapper = vtkPolyDataMapper()
    fn_src >> mapper
    actor = vtkActor(mapper=mapper)
    actor.property.color = colors.GetColor3d('SandyBrown')

    # Create a renderer, render window, and interactor.
    renderer = vtkRenderer(background=colors.GetColor3d('MidnightBlue'))
    render_window = vtkRenderWindow(size=(600, 600), window_name='LegendScaleActor')
    render_window.AddRenderer(renderer)
    render_window_interactor = vtkRenderWindowInteractor()
    render_window_interactor.render_window = render_window

    legend_scale_actor = vtkLegendScaleActor()

    axes_prop = legend_scale_actor.axes_property
    axes_prop.color = colors.GetColor3d('Red')

    legend_scale_actor.top_axis.property = axes_prop
    legend_scale_actor.left_axis.property = axes_prop
    legend_scale_actor.bottom_axis.property = axes_prop

    # Add the actor to the scene.
    renderer.AddActor(actor)
    renderer.AddActor(legend_scale_actor)

    # Render and interact.
    render_window.Render()
    render_window_interactor.Start()


if __name__ == '__main__':
    main()
