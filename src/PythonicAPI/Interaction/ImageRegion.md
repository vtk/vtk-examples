### Description

This example shows how to get the image coordinates of the corners of a BorderWidget.

A perspective projection (`-p`) can be used instead of the parallel camera projection. If this is done, the borders of the widget can appear to be outside the image bounds.
