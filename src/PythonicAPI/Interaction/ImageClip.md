### Description

This example loads an image into the left half of the window. When you move the border widget (the green square in the bottom left corner) over the image, the region that is selected is displayed in the right half of the window.

A perspective projection (`-p`) can be used instead of the parallel camera projection. If this is done, the borders of the widget can appear to be outside the image bounds.
