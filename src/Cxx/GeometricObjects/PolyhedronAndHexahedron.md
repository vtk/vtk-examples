### Description

This is a demonstration of how to construct an unstructured grid containing mixed cells types, polyhedron and hexahedron.
It rely on the `SetPolyhedralCells` API. The resulting object is then displayed.
