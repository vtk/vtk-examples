#include <vtkActor.h>
#include <vtkCameraOrientationWidget.h>
#include <vtkDataSetMapper.h>
#include <vtkGoldenBallSource.h>
#include <vtkInteractorStyleSwitch.h>
#include <vtkNamedColors.h>
#include <vtkNew.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>

#include <array>

int main(int, char*[])
{
  vtkNew<vtkNamedColors> colors;
  colors->SetColor("ParaViewBkg",
                   std::array<unsigned char, 4>{82, 87, 110, 255}.data());

  // Create a golden ball source.
  vtkNew<vtkGoldenBallSource> goldenBallSource;
  goldenBallSource->SetCenter(0.0, 0.0, 0.0);
  goldenBallSource->SetRadius(5.0);
  goldenBallSource->SetResolution(200);
  goldenBallSource->GenerateNormalsOn();
  goldenBallSource->IncludeCenterPointOn();

  vtkNew<vtkDataSetMapper> mapper;
  mapper->SetInputConnection(goldenBallSource->GetOutputPort());

  vtkNew<vtkProperty> actorProp;
  actorProp->SetColor(colors->GetColor3d("Gold").GetData());
  actorProp->SetEdgeColor(colors->GetColor3d("DarkSlateBlue").GetData());
  actorProp->EdgeVisibilityOn();

  vtkNew<vtkActor> actor;
  actor->SetProperty(actorProp);
  actor->SetMapper(mapper);
  actor->GetProperty()->SetColor(colors->GetColor3d("Gold").GetData());

  vtkNew<vtkRenderer> renderer;
  renderer->SetBackground(colors->GetColor3d("ParaViewBkg").GetData());
  vtkNew<vtkRenderWindow> renderWindow;
  renderWindow->SetSize(600, 600);
  renderWindow->SetWindowName("GoldenBallSource");
  renderWindow->AddRenderer(renderer);
  vtkNew<vtkRenderWindowInteractor> renderWindowInteractor;
  renderWindowInteractor->SetRenderWindow(renderWindow);
  auto is = vtkInteractorStyleSwitch::SafeDownCast(
      renderWindowInteractor->GetInteractorStyle());
  if (is)
  {
    is->SetCurrentStyleToTrackballCamera();
  }

  renderer->AddActor(actor);

  vtkNew<vtkCameraOrientationWidget> cow;
  cow->SetParentRenderer(renderer);
  // Enable the widget.
  cow->On();

  renderWindow->Render();
  renderWindowInteractor->Start();

  return EXIT_SUCCESS;
}
