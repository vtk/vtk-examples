### Description

Generate a sphere and position a cone at the center of each cell on the sphere, aligned with the face normal.
 
Taken from [VTK 9.4: A Step Closer to the Ways of Python](https://www.kitware.com/vtk-9-4-a-step-closer-to-the-ways-of-python/)
