#include <vtkActor.h>
#include <vtkCameraOrientationWidget.h>
#include <vtkInteractorStyleSwitch.h>
#include <vtkNamedColors.h>
#include <vtkNew.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkSphereSource.h>

#include <array>

int main(int, char*[])
{
  vtkNew<vtkNamedColors> colors;
  colors->SetColor("ParaViewBkg",
                   std::array<unsigned char, 4>{82, 87, 110, 255}.data());

  // Create a sphere
  vtkNew<vtkSphereSource> sphereSource;
  sphereSource->SetCenter(0.0, 0.0, 0.0);
  sphereSource->SetRadius(5.0);
  // Make the surface smooth.
  sphereSource->SetPhiResolution(50);
  sphereSource->SetThetaResolution(50);
  sphereSource->LatLongTessellationOff();

  vtkNew<vtkPolyDataMapper> mapper;
  mapper->SetInputConnection(sphereSource->GetOutputPort());

  vtkNew<vtkProperty> actorProp;
  actorProp->SetColor(colors->GetColor3d("Peru").GetData());
  actorProp->SetEdgeColor(colors->GetColor3d("DarkSlateBlue").GetData());
  actorProp->EdgeVisibilityOn();

  vtkNew<vtkActor> actor;
  actor->SetProperty(actorProp);
  actor->SetMapper(mapper);

  vtkNew<vtkRenderer> renderer;
  vtkNew<vtkRenderWindow> renderWindow;
  renderWindow->SetSize(600, 600);
  renderWindow->SetWindowName("SphereSource");
  renderWindow->AddRenderer(renderer);
  vtkNew<vtkRenderWindowInteractor> renderWindowInteractor;
  renderWindowInteractor->SetRenderWindow(renderWindow);
  auto is = vtkInteractorStyleSwitch::SafeDownCast(
      renderWindowInteractor->GetInteractorStyle());
  if (is)
  {
    is->SetCurrentStyleToTrackballCamera();
  }

  renderer->AddActor(actor);
  renderer->SetBackground(colors->GetColor3d("ParaViewBkg").GetData());

  vtkNew<vtkCameraOrientationWidget> cow;
  cow->SetParentRenderer(renderer);
  // Enable the widget.
  cow->On();

  renderWindow->Render();
  renderWindowInteractor->Start();

  return EXIT_SUCCESS;
}
