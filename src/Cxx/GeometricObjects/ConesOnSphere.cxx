#include <vtkActor.h>
#include <vtkCameraOrientationWidget.h>
#include <vtkCellCenters.h>
#include <vtkConeSource.h>
#include <vtkGlyph3D.h>
#include <vtkInteractorStyleSwitch.h>
#include <vtkNamedColors.h>
#include <vtkNew.h>
#include <vtkPolyDataMapper.h>
#include <vtkPolyDataNormals.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkSphereSource.h>

#include <array>

int main(int, char*[])
{
  vtkNew<vtkNamedColors> colors;
  colors->SetColor("ParaViewBkg",
                   std::array<unsigned char, 4>{82, 87, 110, 255}.data());

  vtkNew<vtkSphereSource> sphereSource;
  sphereSource->SetCenter(0.0, 0.0, 0.0);
  sphereSource->SetRadius(0.5);
  sphereSource->SetPhiResolution(16);
  sphereSource->SetThetaResolution(16);
  sphereSource->LatLongTessellationOff();

  vtkNew<vtkConeSource> coneSource;
  coneSource->SetCenter(0.0, 0.0, 0.0);
  coneSource->SetRadius(0.08);
  coneSource->SetHeight(0.03);
  coneSource->SetResolution(30);

  vtkNew<vtkPolyDataNormals> normals;
  normals->ComputeCellNormalsOn();
  normals->SetInputConnection(sphereSource->GetOutputPort());

  vtkNew<vtkCellCenters> cellCenters;
  cellCenters->SetInputConnection(normals->GetOutputPort());

  vtkNew<vtkGlyph3D> glyphFilter;
  glyphFilter->OrientOn();
  glyphFilter->SetVectorModeToUseNormal();
  glyphFilter->SetSourceConnection(coneSource->GetOutputPort());
  glyphFilter->SetInputConnection(cellCenters->GetOutputPort());

  vtkNew<vtkPolyDataMapper> mapper;
  mapper->SetInputConnection(glyphFilter->GetOutputPort());

  vtkNew<vtkProperty> actorProp;
  actorProp->SetColor(colors->GetColor3d("Peru").GetData());
  actorProp->SetEdgeColor(colors->GetColor3d("DarkSlateBlue").GetData());
  actorProp->EdgeVisibilityOff();

  vtkNew<vtkActor> actor;
  actor->SetProperty(actorProp);
  actor->SetMapper(mapper);

  vtkNew<vtkRenderer> renderer;
  vtkNew<vtkRenderWindow> renderWindow;
  renderWindow->SetSize(600, 600);
  renderWindow->SetWindowName("ConesOnSphere");
  renderWindow->AddRenderer(renderer);
  vtkNew<vtkRenderWindowInteractor> renderWindowInteractor;
  renderWindowInteractor->SetRenderWindow(renderWindow);
  auto is = vtkInteractorStyleSwitch::SafeDownCast(
      renderWindowInteractor->GetInteractorStyle());
  if (is)
  {
    is->SetCurrentStyleToTrackballCamera();
  }

  renderer->AddActor(actor);
  renderer->SetBackground(colors->GetColor3d("ParaViewBkg").GetData());

  vtkNew<vtkCameraOrientationWidget> cow;
  cow->SetParentRenderer(renderer);
  // Enable the widget.
  cow->On();

  renderWindow->Render();
  renderWindowInteractor->Start();

  return EXIT_SUCCESS;
}
