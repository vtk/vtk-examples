#include <vtkCellData.h>
#include <vtkIdTypeArray.h>
#include <vtkNew.h>
#include <vtkPointData.h>
#include <vtkSphereSource.h>
#include <vtkVersion.h>

#if VTK_VERSION_NUMBER >= 89000000000ULL
#define VTK890 1
#endif

// vtkGenerateIds was introduced in VTK build version 20240504
#if VTK_BUILD_VERSION >= 20240504
#define USE_USE_GENERATE_IDS
#include <vtkGenerateIds.h>
#else
#include <vtkIdFilter.h>
#endif

#include <iostream>

int main(int, char*[])
{
  vtkNew<vtkSphereSource> sphereSource;
  sphereSource->Update();

  std::cout << "There are " << sphereSource->GetOutput()->GetNumberOfPoints()
            << " points." << std::endl;
  std::cout << "There are " << sphereSource->GetOutput()->GetNumberOfCells()
            << " cells." << std::endl;

#ifdef USE_USE_GENERATE_IDS
  vtkNew<vtkGenerateIds> idFilter;
#else
  vtkNew<vtkIdFilter> idFilter;
#endif
  idFilter->SetInputConnection(sphereSource->GetOutputPort());
#if VTK890
  idFilter->SetPointIdsArrayName("ids");
  idFilter->SetCellIdsArrayName("ids");
#else
  idFilter->SetIdsArrayName("ids");
#endif
  idFilter->Update();

#ifdef USE_USE_GENERATE_IDS
  std::cout << "Point Array Names: " << std::endl;
  for (vtkIdType i = 0;
       i < idFilter->GetPolyDataOutput()->GetPointData()->GetNumberOfArrays();
       i++)
  {
    std::cout << "  " << i << ": "
              << idFilter->GetPolyDataOutput()->GetPointData()->GetArrayName(i)
              << std::endl;
  }

  std::cout << "Cell Array Names: " << std::endl;
  for (vtkIdType i = 0;
       i < idFilter->GetPolyDataOutput()->GetCellData()->GetNumberOfArrays();
       i++)
  {
    std::cout << "  " << i << ": "
              << idFilter->GetPolyDataOutput()->GetCellData()->GetArrayName(i)
              << std::endl;
  }

  vtkIdTypeArray* pointIds = dynamic_cast<vtkIdTypeArray*>(
      idFilter->GetPolyDataOutput()->GetPointData()->GetArray("ids"));
  std::cout << "There are " << pointIds->GetNumberOfTuples() << " point ids"
            << "." << std::endl;

  vtkIdTypeArray* cellIds = dynamic_cast<vtkIdTypeArray*>(
      idFilter->GetPolyDataOutput()->GetCellData()->GetArray("ids"));
  std::cout << "There are " << cellIds->GetNumberOfTuples() << " cell ids"
            << "." << std::endl;
#else
  std::cout << "point arrays: " << std::endl;
  for (vtkIdType i = 0;
       i < idFilter->GetOutput()->GetPointData()->GetNumberOfArrays(); i++)
  {
    std::cout << idFilter->GetOutput()->GetPointData()->GetArrayName(i)
              << std::endl;
  }

  std::cout << "cell arrays: " << std::endl;
  for (vtkIdType i = 0;
       i < idFilter->GetOutput()->GetCellData()->GetNumberOfArrays(); i++)
  {
    std::cout << idFilter->GetOutput()->GetCellData()->GetArrayName(i)
              << std::endl;
  }

  vtkIdTypeArray* pointIds = dynamic_cast<vtkIdTypeArray*>(
      idFilter->GetOutput()->GetPointData()->GetArray("ids"));
  std::cout << "There are " << pointIds->GetNumberOfTuples() << " point ids"
            << std::endl;

  vtkIdTypeArray* cellIds = dynamic_cast<vtkIdTypeArray*>(
      idFilter->GetOutput()->GetCellData()->GetArray("ids"));
  std::cout << "There are " << cellIds->GetNumberOfTuples() << " cell ids"
            << std::endl;
#endif

  return EXIT_SUCCESS;
}
