#include <vtkActor.h>
#include <vtkDelaunay2D.h>
#include <vtkNamedColors.h>
#include <vtkNew.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkVertexGlyphFilter.h>

int main(int, char*[])
{
  vtkNew<vtkNamedColors> colors;

  // Create a set of heights on a grid.
  // This is often called a "terrain map".
  vtkNew<vtkPoints> points;

  unsigned int gridSize = 10;
  for (unsigned int x = 0; x < gridSize; x++)
  {
    for (unsigned int y = 0; y < gridSize; y++)
    {
      points->InsertNextPoint(x, y, (x + y) / (y + 1));
    }
  }

  // Add the grid points to a polydata object.
  vtkNew<vtkPolyData> polydata;
  polydata->SetPoints(points);

  // Triangulate the grid points
  vtkNew<vtkDelaunay2D> delaunay;
  delaunay->SetInputData(polydata);

  // Visualize
  vtkNew<vtkPolyDataMapper> meshMapper;
  meshMapper->SetInputConnection(delaunay->GetOutputPort());

  vtkNew<vtkActor> meshActor;
  meshActor->SetMapper(meshMapper);
  meshActor->GetProperty()->SetColor(
      colors->GetColor3d("LightGoldenrodYellow").GetData());
  meshActor->GetProperty()->EdgeVisibilityOn();
  meshActor->GetProperty()->SetEdgeColor(
      colors->GetColor3d("CornflowerBlue").GetData());
  meshActor->GetProperty()->SetLineWidth(3);
  meshActor->GetProperty()->RenderLinesAsTubesOn();

  vtkNew<vtkVertexGlyphFilter> glyphFilter;
  glyphFilter->SetInputData(polydata);

  vtkNew<vtkPolyDataMapper> pointMapper;
  pointMapper->SetInputConnection(glyphFilter->GetOutputPort());

  vtkNew<vtkActor> pointActor;
  pointActor->SetMapper(pointMapper);
  pointActor->GetProperty()->SetColor(colors->GetColor3d("DeepPink").GetData());
  pointActor->GetProperty()->SetPointSize(10);
  pointActor->GetProperty()->RenderPointsAsSpheresOn();

  vtkNew<vtkRenderer> renderer;
  renderer->SetBackground(colors->GetColor3d("PowderBlue").GetData());
  vtkNew<vtkRenderWindow> renderWindow;
  renderWindow->SetSize(600, 600);
  renderWindow->SetWindowName("Delaunay2D");
  renderWindow->AddRenderer(renderer);
  vtkNew<vtkRenderWindowInteractor> renderWindowInteractor;
  renderWindowInteractor->SetRenderWindow(renderWindow);

  renderer->AddActor(meshActor);
  renderer->AddActor(pointActor);

  renderWindow->Render();
  renderWindowInteractor->Start();

  return EXIT_SUCCESS;
}
