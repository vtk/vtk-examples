#include <vtkActor.h>
#include <vtkCamera.h>
#include <vtkCameraOrientationWidget.h>
#include <vtkInteractorStyleSwitch.h>
#include <vtkNamedColors.h>
#include <vtkNew.h>
#include <vtkParametricEnneper.h>
#include <vtkParametricFunctionSource.h>
#include <vtkPolarAxesActor.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkProperty2D.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>

#include <cmath>
#include <numeric>
#include <vector>

int main(int, char*[])
{
  vtkNew<vtkNamedColors> colors;

  // The source will be a parametric function.
  vtkNew<vtkParametricEnneper> src;
  vtkNew<vtkParametricFunctionSource> fnSrc;
  fnSrc->SetParametricFunction(src);
  fnSrc->Update();

  // Create a mapper and actor.
  vtkNew<vtkPolyDataMapper> mapper;
  mapper->SetInputConnection(fnSrc->GetOutputPort());
  vtkNew<vtkActor> actor;
  actor->SetMapper(mapper);
  actor->GetProperty()->SetColor(
      colors->GetColor3d("CornflowerBlue").GetData());

  // Create a renderer, render window, and interactor.
  vtkNew<vtkRenderer> renderer;
  vtkNew<vtkRenderWindow> renderWindow;
  renderWindow->AddRenderer(renderer);
  renderWindow->SetWindowName("PolarAxesActor");
  renderWindow->SetSize(600, 600);

  vtkNew<vtkRenderWindowInteractor> renderWindowInteractor;
  renderWindowInteractor->SetRenderWindow(renderWindow);
  auto is = vtkInteractorStyleSwitch::SafeDownCast(
      renderWindowInteractor->GetInteractorStyle());
  if (is)
  {
    is->SetCurrentStyleToTrackballCamera();
  }

  double* bounds;
  bounds = fnSrc->GetOutput()->GetBounds();
  std::vector<double> p1{bounds[0], bounds[2]};
  std::vector<double> p2{bounds[1], bounds[3]};
  auto radius =
      std::inner_product(std::cbegin(p1), std::cend(p1), std::begin(p2), 0.0);
  radius = std::sqrt(std::abs(radius));
  std::vector<double> pole{0.0, 0.0, bounds[5]};

  vtkNew<vtkPolarAxesActor> polarAxesActor;
  polarAxesActor->SetPole(pole.data());
  polarAxesActor->SetMaximumRadius(radius);
  polarAxesActor->SetRequestedNumberOfPolarAxes(5);
  polarAxesActor->SetMinimumAngle(-45.0);
  polarAxesActor->SetMaximumAngle(135.0);

  auto axesProp = polarAxesActor->GetPolarAxisProperty();
  axesProp->SetColor(colors->GetColor3d("Red").GetData());
  auto arcsProp = polarAxesActor->GetPolarArcsProperty();
  arcsProp->SetColor(colors->GetColor3d("Yellow").GetData());
  arcsProp->SetLineWidth(1.0);

  polarAxesActor->SetSecondaryRadialAxesProperty(axesProp);
  polarAxesActor->SetLastRadialAxisProperty(axesProp);
  polarAxesActor->SetSecondaryPolarArcsProperty(arcsProp);

  // Add the actor to the scene.
  renderer->AddActor(actor);
  renderer->AddActor(polarAxesActor);

  renderer->GetActiveCamera()->ParallelProjectionOn();
  polarAxesActor->SetCamera(renderer->GetActiveCamera());
  renderer->ResetCamera();

  vtkNew<vtkCameraOrientationWidget> cow;
  cow->SetParentRenderer(renderer);
  // Enable the widget.
  cow->On();

  renderer->SetBackground(colors->GetColor3d("MidnightBlue").GetData());

  // Render and interact.
  renderWindow->Render();
  renderWindowInteractor->Start();

  return EXIT_SUCCESS;
}
