# PythonicAPI Examples

!!! Warning
    These examples only work with VTK Version: 9.3.20240428 or greater.

These examples:

- Use the improved VTK Python interface. Some information about the improved Python interface can be found [here](../PythonicAPIComments/)
- Are either newly crafted examples or upgrades of existing Python examples

See:

- [More Pythonic VTK wrapping](https://discourse.vtk.org/t/more-pythonic-vtk-wrapping/13092) for the VTK Discourse discussion
- [Wrap VTK properties to pythonic properties with snake_case names](https://gitlab.kitware.com/vtk/vtk/-/merge_requests/10820) for the merge request

## Upgrading an existing example to use the improved VTK Python interface

1. Copy the example from the **src/Python** folder into the **src/PythonicAPI** folder maintaining the same path structure. If there is a corresponding markdown file, copy it.
2. Copy the corresponding test image from **src/Testing/Baseline/Python/** into **src/Testing/Baseline/PythonicAPI/**
3. Edit **src/PythonicAPI.md**, possibly creating a table and headings to match the original example in **src/Python**.
4. Upgrade the Python example.
5. The associated markdown file (if any) may need checking to ensure any links in the document remain valid.
6. Check everything is working and do a Merge Request.

## Adding a new example

Follow the documented procedure [ForDevelopers](https://examples.vtk.org/site/Instructions/ForDevelopers/) remembering that the folder to use is **PythonicAPI**.

## VTK Classes Summary

This Python script, [SelectExamples](../PythonicAPI/Utilities/SelectExamples), will let you select examples based on a VTK Class and language. It requires Python 3.7 or later. The following tables will produce similar information.

- [VTK Classes with Examples](/Coverage/PythonicAPIVTKClassesUsed.md), this table is really useful when searching for example(s) using a particular class.

- [VTK Classes with No Examples](/Coverage/PythonicAPIVTKClassesNotUsed.md), please add examples in your area of expertise!

## Tutorials

## Hello World

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[A hello world example](/PythonicAPI/GeometricObjects/CylinderExample) | Cylinder example from the VTK Textbook and source code. A hello world example.

## Simple Operations

| Example Name | Description | Image |
| -------------- | ------------- | -------- |
[DistanceBetweenPoints](/PythonicAPI/SimpleOperations/DistanceBetweenPoints) | Distance between two points.
[DistancePointToLine](/PythonicAPI/SimpleOperations/DistancePointToLine) | Distance between a point and a line.
[GaussianRandomNumber](/PythonicAPI/SimpleOperations/GaussianRandomNumber) | Generates Gaussian random numbers.
[PerspectiveTransform](/PythonicAPI/SimpleOperations/PerspectiveTransform) | Apply a perspective transformation to a point.


## Input and Output

### Graph Formats

### 3D File Formats

| Example Name | Description | Image |
| -------------- | ------------- | -------- |
[XGMLReader](/PythonicAPI/InfoVis/XGMLReader) | Read a .gml file

#### Standard Formats

##### Input

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[ParticleReader](/PythonicAPI/IO/ParticleReader) | This example reads ASCII files where each line consists of points with its position (x,y,z) and (optionally) one scalar or binary files in RAW 3d file format.
[ReadAllPolyDataTypesDemo](/PythonicAPI/IO/ReadAllPolyDataTypesDemo) | Read all VTK polydata file types.
[ReadCML](/PythonicAPI/IO/ReadCML) | Read Chemistry Markup Language files.
[ReadExodusData](/PythonicAPI/IO/ReadExodusData) | A simple script for reading and viewing ExodusII data interactively.
[ReadSLC](/PythonicAPI/IO/ReadSLC) | Read an SLC file.
[TransientHDFReader](/PythonicAPI/IO/TransientHDFReader) | Read transient data written inside a vtkhdf file.

###### Importers

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[3DSImporter](/PythonicAPI/IO/3DSImporter) | Import a 3D Studio scene that includes multiple actors.
[ImportPolyDataScene](/PythonicAPI/IO/ImportPolyDataScene) | Import a polydata scene using multiblock datasets.
[ImportToExport](/PythonicAPI/IO/ImportToExport)| Import a scene and optionally export the scene.

##### Output

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[DelimitedTextWriter](/PythonicAPI/InfoVis/DelimitedTextWriter) | Write data to a delimited file.
[WritePLY](/PythonicAPI/IO/WritePLY) |
[WriteSTL](/PythonicAPI/IO/WriteSTL) |

###### Exporters

#### VTK Formats

##### Input

##### Output

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[ExodusIIWriter](/PythonicAPI/Parallel/ExodusIIWriter) | Write a time varying ExodusII file.

#### Legacy VTK Formats

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[GenericDataObjectReader](/PythonicAPI/IO/GenericDataObjectReader) | Read any type of legacy .vtk file.
[ReadLegacyUnstructuredGrid](/PythonicAPI/IO/ReadLegacyUnstructuredGrid) | Read an unstructured grid that contains 11 linear cells.
[WriteLegacyLinearCells](/PythonicAPI/IO/WriteLegacyLinearCells) | Write each linear cell into a legacy UnstructuredGrid file (.vtk).
[WriteXMLLinearCells](/PythonicAPI/IO/WriteXMLLinearCells) | Write each linear cell into an XML UnstructuredGrid file (.vtu).

### Image Format

#### Input

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[HDRReader](/PythonicAPI/IO/HDRReader) | Read a high-dynamic-range imaging file.
[ReadDICOM](/PythonicAPI/IO/ReadDICOM) | Read DICOM file.
[ReadDICOMSeries](/PythonicAPI/IO/ReadDICOMSeries) | This example demonstrates how to read a series of DICOM images and scroll through slices


#### Output

## Geometric Objects

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[Dodecahedron](/PythonicAPI/GeometricObjects/Dodecahedron) | Create a dodecahedron using vtkPolyhedron.
[GeometricObjectsDemo](/PythonicAPI/GeometricObjects/GeometricObjectsDemo) |
[PipelineReuse](/PythonicAPI/GeometricObjects/PipelineReuse) | How to reuse a pipeline.
[PlaneSourceDemo](/PythonicAPI/GeometricObjects/PlaneSourceDemo) | Display the instance variables that define a vtkPlaneSource.
[Planes](/PythonicAPI/GeometricObjects/Planes) | We create a convex hull of the planes for display purposes.
[PlanesIntersection](/PythonicAPI/GeometricObjects/PlanesIntersection) |
[SourceObjectsDemo](/PythonicAPI/GeometricObjects/SourceObjectsDemo) | Examples of source objects that procedurally generate polygonal models.  These nine images represent just some of the capability of VTK. From upper left in reading order: sphere, cone, cylinder, cube, plane, text, random point cloud, disk (with or without hole), and line source. Other polygonal source objects are available; check subclasses of vtkPolyDataAlgorithm.

### Cells

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[CellTypeSource](/PythonicAPI/GeometricObjects/CellTypeSource) | Generate tessellated cells.
[ConvexPointSet](/PythonicAPI/GeometricObjects/ConvexPointSet) | Generate a ConvexPointSet cell.
[LinearCellsDemo](/PythonicAPI/GeometricObjects/LinearCellsDemo) | Linear cell types found in VTK. The numbers define the ordering of the points making the cell.
[Polyhedron](/PythonicAPI/GeometricObjects/Polyhedron) | Create an unstructured grid representation of a polyhedron (cube) and write it out to a file.

### Sources

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[ConesOnSphere](/PythonicAPI/GeometricObjects/ConesOnSphere) |Generate a sphere and position a cone at the center of each cell on the sphere, aligned with the face normal.
[EarthSource](/PythonicAPI/GeometricObjects/EarthSource) | Create the Earth.
[Frustum](/PythonicAPI/GeometricObjects/Frustum) |
[GoldenBallSource](/PythonicAPI/GeometricObjects/GoldenBallSource) | Construct a solid, tetrahedralized ball.
[OrientedArrow](/PythonicAPI/GeometricObjects/OrientedArrow) | Orient an arrow along an arbitrary vector.
[OrientedCylinder](/PythonicAPI/GeometricObjects/OrientedCylinder) | Orient a cylinder along an arbitrary vector.
[PlatonicSolids](/PythonicAPI/GeometricObjects/PlatonicSolids) | All five platonic solids are displayed.
[SphereSource](/PythonicAPI/GeometricObjects/SphereSource) | Construct a sphere.
[TessellatedBoxSource](/PythonicAPI/GeometricObjects/TessellatedBoxSource) | Generate a box with tessellated sides.

### Non Linear

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[IsoparametricCellsDemo](/PythonicAPI/GeometricObjects/IsoparametricCellsDemo) | Nonlinear isoparametric cell types in VTK.

### Parametric Objects

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[ParametricObjectsDemo](/PythonicAPI/GeometricObjects/ParametricObjectsDemo) | Demonstrates the Parametric classes added by Andrew Maclean and additional classes added by Tim Meehan. The parametric spline is also included. Options are provided to display single objects, add backface, add normals and print out an image.
[ParametricKuenDemo](/PythonicAPI/GeometricObjects/ParametricKuenDemo) | Interactively change the parameters for a Kuen Surface.

## Implicit Functions and Iso-surfaces

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[BooleanOperationImplicitFunctions](/PythonicAPI/ImplicitFunctions/BooleanOperationImplicitFunctions) | Demonstrate booleans of two different implicit functions
[ContourTriangulator](/PythonicAPI/Modelling/ContourTriangulator) | Create a contour from a structured point set (image) and triangulate it.
[DiscreteFlyingEdges3D](/PythonicAPI/Modelling/DiscreteFlyingEdges3D) | Generate surfaces from labeled data.
[ExtractData](/PythonicAPI/VisualizationAlgorithms/ExtractData) | Implicit functions used to select data: Two ellipsoids are combined using the union operation used to select voxels from a volume. Voxels are shrunk 50 percent.
[ExtractLargestIsosurface](/PythonicAPI/Modelling/ExtractLargestIsosurface) | Extract largest isosurface.
[IceCream](/PythonicAPI/VisualizationAlgorithms/IceCream) | How to use boolean combinations of implicit functions to create a model of an ice cream cone.
[ImplicitDataSet](/PythonicAPI/ImplicitFunctions/ImplicitDataSet) | Convert an imagedata to an implicit function.
[ImplicitQuadric](/PythonicAPI/ImplicitFunctions/ImplicitQuadric) | Create an ellipsoid using an implicit quadric
[ImplicitSphere](/PythonicAPI/ImplicitFunctions/ImplicitSphere) | Demonstrate sampling of a sphere implicit function
[ImplicitSphere1](/PythonicAPI/ImplicitFunctions/ImplicitSphere1) | Demonstrate sampling of a sphere implicit function
[IsoContours](/PythonicAPI/ImplicitFunctions/IsoContours) | Visualize different isocontours using a slider.
[Lorenz](/PythonicAPI/Visualization/Lorenz) | Visualizing a Lorenz strange attractor by integrating the Lorenz equations in a volume.
[MarchingCubes](/PythonicAPI/Modelling/MarchingCubes) | Create a voxelized sphere.
[SampleFunction](/PythonicAPI/ImplicitFunctions/SampleFunction) | Sample and visualize an implicit function.
[SmoothDiscreteFlyingEdges3D](/PythonicAPI/Modelling/SmoothDiscreteFlyingEdges3D) | Generate smooth surfaces from labeled data.

## Working with 3D Data

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[AlignTwoPolyDatas](/PythonicAPI/PolyData/AlignTwoPolyDatas) | Align two vtkPolyData's.
[BooleanPolyDataFilters](/PythonicAPI/PolyData/BooleanPolyDataFilters) | This example performs a boolean operation (intersection, union or difference) of two PolyData using either a vtkBooleanOperationPolyDataFilter or a vtkLoopBooleanPolyDataFilter
[Bottle](/PythonicAPI/Modelling/Bottle) | Model a rotationally symmetric object.
[CappedSphere](/PythonicAPI/Modelling/CappedSphere) | Rotate an arc to create a capped sphere.
[CellsInsideObject](/PythonicAPI/PolyData/CellsInsideObject) | Extract cells inside a closed surface.
[CenterOfMass](/PythonicAPI/PolyData/CenterOfMass) | Compute the center of mass of the points.
[ConnectivityFilter](/PythonicAPI/Filtering/ConnectivityFilter) | Color any dataset type based on connectivity.
[Curvatures](/PythonicAPI/PolyData/Curvatures) | Compute Gaussian, and Mean Curvatures.
[CurvaturesAdjustEdges](/PythonicAPI/PolyData/CurvaturesAdjustEdges) | Get the Gaussian and Mean curvatures of a surface with adjustments for edge effects.
[DecimatePolyline](/PythonicAPI/PolyData/DecimatePolyline) | Decimate polyline.
[DistancePolyDataFilter](/PythonicAPI/PolyData/DistancePolyDataFilter) | Compute the distance function from one vtkPolyData to another.
[ExternalContour](/PythonicAPI/PolyData/ExternalContour) | Get the external contour of a PolyData object.
[ExtractPolyLinesFromPolyData](/PythonicAPI/PolyData/ExtractPolyLinesFromPolyData) | Extract polylines from polydata.
[ExtractSelection](/PythonicAPI/PolyData/ExtractSelection) |Extract selected points.
[ExtractSelectionOriginalId](/PythonicAPI/PolyData/ExtractSelectionOriginalId) | Extract selection and find the correspondence between the new and original ids.
[ExtractSelectionUsingCells](/PythonicAPI/PolyData/ExtractSelectionUsingCells) | Extract cell, select cell.
[ExtractSelectionUsingPoints](/PythonicAPI/PolyData/ExtractSelectionUsingPoints) | Extract points but bring cells that are still complete with them.
[ExtractVisibleCells](/PythonicAPI/Filtering/ExtractVisibleCells) | Extract and highlight visible cells.
[Finance](/PythonicAPI/Modelling/Finance) | Visualization of multidimensional financial data. The gray/wireframe surface represents the total data population. The red surface represents data points delinquent on loan payment.
[FinanceFieldData](/PythonicAPI/Modelling/FinanceFieldData) | Visualization of multidimensional financial data. The yellow surface represents the total data population. The red surface represents data points delinquent on loan payment.
[FitSplineToCutterOutput](/PythonicAPI/PolyData/FitSplineToCutterOutput) | Fit a spline to cutter output.
[Glyph2D](/PythonicAPI/Filtering/Glyph2D) |
[GradientFilter](/PythonicAPI/PolyData/GradientFilter) | Compute the gradient of a scalar field on a data set.
[ImplicitPolyDataDistance](/PythonicAPI/PolyData/ImplicitPolyDataDistance) |
[IntersectionPolyDataFilter](/PythonicAPI/PolyData/IntersectionPolyDataFilter) | Compute the intersection of two vtkPolyData objects.
[KMeansClustering](/PythonicAPI/InfoVis/KMeansClustering) | KMeans Clustering
[LineOnMesh](/PythonicAPI/DataManipulation/LineOnMesh) | Plot a spline on a terrain-like surface.
[KochanekSplineDemo](/PythonicAPI/PolyData/KochanekSplineDemo) | Interactively change the parameters of the Kochanek spline.
[MergeSelections](/PythonicAPI/PolyData/MergeSelections) | Merge selected points.
[MeshLabelImageColor](/PythonicAPI/DataManipulation/MeshLabelImageColor) | Mesh a single label from a label image. Then smooth and color the vertices according to the displacement error introduced by the smoothing.
[MultiBlockMergeFilter](/PythonicAPI/PolyData/MultiBlockMergeFilter) | Combine MultiBlockDataSets.
[OrientedBoundingCylinder](/PythonicAPI/PolyData/OrientedBoundingCylinder) | Create an oriented cylinder that encloses a vtkPolyData. The axis of the cylinder is aligned with the longest axis of the vtkPolyData.
[Outline](/PythonicAPI/PolyData/Outline) | Draw the bounding box of the data.
[PerlinNoise](/PythonicAPI/Filtering/PerlinNoise) |
[PolyDataContourToImageData](/PythonicAPI/PolyData/PolyDataContourToImageData) |
[PolyDataToImageDataStencil](/PythonicAPI/PolyData/PolyDataToImageDataStencil) |
[ResamplePolyLine](/PythonicAPI/PolyData/ResamplePolyLine) |
[RuledSurfaceFilter](/PythonicAPI/PolyData/RuledSurfaceFilter) |
[Silhouette](/PythonicAPI/PolyData/Silhouette) |
[SmoothMeshGrid](/PythonicAPI/PolyData/SmoothMeshGrid) | Create a terrain with regularly spaced points and smooth it with ?vtkLoopSubdivisionFilter? and ?vtkButterflySubdivisionFilter?.
[SpatioTemporalHarmonicsSource](/PythonicAPI/Filtering/SpatioTemporalHarmonicsSource) | Generate image data containing spatio-temporal harmonic data.
[Spring](/PythonicAPI/Modelling/Spring) | Rotation in combination with linear displacement and radius variation.
[ThinPlateSplineTransform](/PythonicAPI/PolyData/ThinPlateSplineTransform) |
[VertexConnectivity](/PythonicAPI/PolyData/VertexConnectivity) | Get a list of vertices attached (through an edge) to a vertex.
[WarpTo](/PythonicAPI/Filtering/WarpTo) | Deform geometry by warping towards a point.
[WarpVector](/PythonicAPI/PolyData/WarpVector) | This example warps/deflects a line.

### Data Types

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[CompositePolyDataMapper](/PythonicAPI/CompositeData/CompositePolyDataMapper) |
[OverlappingAMR](/PythonicAPI/CompositeData/OverlappingAMR) | Demonstrates how to create and populate VTK's Overlapping AMR Grid type with data.

### Data Type Conversions

### Point Cloud Operations

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[DensifyPoints](/PythonicAPI/Points/DensifyPoints) | Add points to a point cloud.
[ExtractClusters](/PythonicAPI/Points/ExtractClusters) | From a set of randomly distributed spheres, extract connected clusters.
[ExtractPointsDemo](/PythonicAPI/Points/ExtractPointsDemo) | Extract points inside an implicit function.
[ExtractSurface](/PythonicAPI/Points/ExtractSurface) | Create a surface from unorganized points using point filters.
[ExtractSurfaceDemo](/PythonicAPI/Points/ExtractSurfaceDemo) | Create a surface from unorganized points using point filters (DEMO).
[FitImplicitFunction](/PythonicAPI/Points/FitImplicitFunction) | Extract points within a distance to an implicit function.
[MaskPointsFilter](/PythonicAPI/Points/MaskPointsFilter) | Extract points within an image mask.
[SignedDistance](/PythonicAPI/Points/SignedDistance) | Compute signed distance to a point cloud.
[UnsignedDistance](/PythonicAPI/Points/UnsignedDistance) | Compute unsigned distance to a point cloud.

### Working with Meshes

This section includes examples of manipulating meshes.

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[ClosedSurface](/PythonicAPI/PolyData/ClosedSurface) | Check if a surface is closed.
[DeformPointSet](/PythonicAPI/Meshes/DeformPointSet) | Use the vtkDeformPointSet filter to deform a vtkSphereSource with arbitrary polydata.
[DelaunayMesh](/PythonicAPI/Modelling/DelaunayMesh) | Two-dimensional Delaunay triangulation of a random set of points. Points and edges are shown highlighted with sphere glyphs and tubes.
[DijkstraGraphGeodesicPath](/PythonicAPI/PolyData/DijkstraGraphGeodesicPath) | Find the shortest path between two points on a mesh.
[ElevationFilter](/PythonicAPI/Meshes/ElevationFilter) | Color a mesh by height.
[FillHoles](/PythonicAPI/Meshes/FillHoles) | Close holes in a mesh.
[FitToHeightMap](/PythonicAPI/Meshes/FitToHeightMap) | Drape a polydata over an elevation map.
[GreedyTerrainDecimation](/PythonicAPI/PolyData/GreedyTerrainDecimation) | Create a mesh from an ImageData
[HighlightBadCells](/PythonicAPI/PolyData/HighlightBadCells) |
[IdentifyHoles](/PythonicAPI/Meshes/IdentifyHoles) | Close holes in a mesh and identify the holes.
[ImplicitSelectionLoop](/PythonicAPI/PolyData/ImplicitSelectionLoop) | Select a region of a mesh with an implicit function.
[InterpolateFieldDataDemo](/PythonicAPI/Meshes/InterpolateFieldDataDemo) | Resample a fine grid and interpolate field data.
[MeshQuality](/PythonicAPI/PolyData/MeshQuality) |
[MatrixMathFilter](/PythonicAPI/Meshes/MatrixMathFilter) | Compute various quantities on cells and points in a mesh.
[OBBDicer](/PythonicAPI/Meshes/OBBDicer) | Breakup a mesh into pieces.
[PointInterpolator](/PythonicAPI/Meshes/PointInterpolator) | Plot a scalar field of points onto a PolyData surface.
[SplitPolyData](/PythonicAPI/Meshes/SplitPolyData) | Breakup a mesh into pieces and save the pieces into files


#### Clipping

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[BoxClipStructuredPoints](/PythonicAPI/Visualization/BoxClipStructuredPoints) | Clip vtkStructuredPoints with a box. The results are unstructured grids with tetrahedra.
[BoxClipUnstructuredGrid](/PythonicAPI/Visualization/BoxClipUnstructuredGrid) | Clip a vtkUnstructuredGrid with a box. The results are unstructured grids with tetrahedra.
[ClipClosedSurface](/PythonicAPI/Meshes/ClipClosedSurface) | Clip a surface with multiple planes.
[ClipDataSetWithPolyData](/PythonicAPI/Meshes/ClipDataSetWithPolyData) | Clip a vtkRectilinearGrid with arbitrary polydata. In this example, use a vtkConeSource to generate polydata to slice the grid, resulting in an unstructured grid.
[ClipDataSetWithPolyData1](/PythonicAPI/Meshes/ClipDataSetWithPolyData1) | Clip a vtkRectilinearGrid with arbitrary polydata. In this example, use a vtkConeSource to generate polydata to slice the grid, resulting in an unstructured grid.
[ImplicitDataSetClipping](/PythonicAPI/PolyData/ImplicitDataSetClipping) | Clip using an implicit data set.
[SolidClip](/PythonicAPI/Meshes/SolidClip) | Create a "solid" clip. The "ghost" of the part clipped away is also shown.
[TableBasedClipDataSetWithPolyData2](/PythonicAPI/Meshes/TableBasedClipDataSetWithPolyData2) | Clip a vtkRectilinearGrid with a checkerboard pattern.

### Working with Structured 3D Data

#### ?vtkImageData?

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[ClipVolume](/PythonicAPI/ImageData/ClipVolume) | Clip a volume and produce a vtkUnhstructuredGrid.
[ImageIterator](/PythonicAPI/ImageData/ImageIterator) |
[ImageIteratorDemo](/PythonicAPI/ImageData/ImageIteratorDemo) | Demonstrate using indexing to access pixels in a region.
[ImageTranslateExtent](/PythonicAPI/ImageData/ImageTranslateExtent) | Change the extent of a vtkImageData.
[ImageWeightedSum](/PythonicAPI/ImageData/ImageWeightedSum) | Add two or more images.

#### ?vtkExplicitStructuredGrid?

| Example Name | Description | Image |
| ------------ | ----------- | ----- |
[CreateESGrid](/PythonicAPI/ExplicitStructuredGrid/CreateESGrid) | Create an explicit structured grid and convert this to an unstructured grid or vice versa.
[LoadESGrid](/PythonicAPI/ExplicitStructuredGrid/LoadESGrid) | Load a VTU file and convert the dataset to an explicit structured grid.

#### ?vtkStructuredGrid?

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[BlankPoint](/PythonicAPI/StructuredGrid/BlankPoint) | Blank a point of a vtkStructuredGrid.
[SGrid](/PythonicAPI/StructuredGrid/SGrid) | Creating a structured grid dataset of a semi-cylinder. Vectors are created whose magnitude is proportional to radius and oriented in tangential direction.

#### ?vtkStructuredPoints?

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[Vol](/PythonicAPI/StructuredPoints/Vol) | Creating a image data dataset. Scalar data is generated from the equation for a sphere. Volume dimensions are 26 x 26 x 26.

#### ?vtkRectilinearGrid?

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[RGrid](/PythonicAPI/RectilinearGrid/RGrid) | Creating a rectilinear grid dataset. The coordinates along each axis are defined using an instance of vtkDataArray.
[VisualizeRectilinearGrid](/PythonicAPI/RectilinearGrid/VisualizeRectilinearGrid) | Visualize the cells of a rectilinear grid.

### Working with Unstructured 3D Data

This section includes ?vtkUnstructuredGrid?.

#### ?vtkUnstructuredGrid?

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[ClipUnstructuredGridWithPlane](/PythonicAPI/UnstructuredGrid/ClipUnstructuredGridWithPlane) | Clip a UGrid with a plane.
[ClipUnstructuredGridWithPlane2](/PythonicAPI/UnstructuredGrid/ClipUnstructuredGridWithPlane2) | Clip a UGrid with a plane.

### Registration

### Medical

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[GenerateCubesFromLabels](/PythonicAPI/Medical/GenerateCubesFromLabels) | Create cubes from labeled volume data.
[GenerateModelsFromLabels](/PythonicAPI/Medical/GenerateModelsFromLabels) | Create models from labeled volume data.
[MedicalDemo1](/PythonicAPI/Medical/MedicalDemo1) | Create a skin surface from volume data.

### Surface reconstruction

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[Delaunay3DDemo](/PythonicAPI/Modelling/Delaunay3DDemo) | Interactively adjust Alpha for Delaunay3D.

## Utilities

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[BoundingBox](/PythonicAPI/Utilities/BoundingBox) | Bounding Box construction.
[BoundingBoxIntersection](/PythonicAPI/Utilities/BoundingBoxIntersection) | Box intersection and Inside tests.
[CheckVTKVersion](/PythonicAPI/Utilities/CheckVTKVersion) | Check the VTK version and provide alternatives for different VTK versions.
[ClassesInLang1NotInLang2](/PythonicAPI/Utilities/ClassesInLang1NotInLang2) | Select VTK classes with corresponding examples in one language but not in another.
[ConstrainedDelaunay2D](/PythonicAPI/Filtering/ConstrainedDelaunay2D) | Perform a 2D Delaunay triangulation on a point set respecting a specified boundary.
[Delaunay2D](/PythonicAPI/Filtering/Delaunay2D) | Perform a 2D Delaunay triangulation on a point set.
[FileOutputWindow](/PythonicAPI/Utilities/FileOutputWindow) | Write errors to a log file instead of the usual vtk pop-up window.
[JSONColorMapToLUT](/PythonicAPI/Utilities/JSONColorMapToLUT) | Take a JSON description of a colormap and convert it to a VTK colormap.
[ColorMapToLUT](/PythonicAPI/Utilities/ColorMapToLUT) | Use vtkDiscretizableColorTransferFunction to generate a VTK colormap.
[DetermineActorType](/PythonicAPI/Utilities/DetermineActorType) | Determine the type of an actor.
[MassProperties](/PythonicAPI/Utilities/MassProperties) | Compute volume and surface area of a closed, triangulated mesh.
[MultipleRenderWindows](/PythonicAPI/Visualization/MultipleRenderWindows) | Multiple Render Windows.
[MultipleViewports](/PythonicAPI/Visualization/MultipleViewports) | Multiple Viewports.
[OffScreenRendering](/PythonicAPI/Utilities/OffScreenRendering) | Off Screen Rendering.
[PassThrough](/PythonicAPI/InfoVis/PassThrough) | Pass input along to output.
[PCADemo](/PythonicAPI/Utilities/PCADemo) | Project 2D points onto the best 1D subspace using PCA values.
[PCAStatistics](/PythonicAPI/Utilities/PCAStatistics) | Compute Principal Component Analysis (PCA) values.
[RescaleReverseLUT](/PythonicAPI/Utilities/RescaleReverseLUT) | Demonstrate how to adjust a colormap so that the colormap scalar range matches the scalar range on the object. You can optionally reverse the colors.
[ResetCameraOrientation](/PythonicAPI/Utilities/ResetCameraOrientation) | Reset camera orientation to a previously saved orientation.
[SaveSceneToFieldData](/PythonicAPI/Utilities/SaveSceneToFieldData) | Save a vtkCamera's state in a vtkDataSet's vtkFieldData and restore it.
[SaveSceneToFile](/PythonicAPI/Utilities/SaveSceneToFile) | Save a vtkCamera's state in a file and restore it.
[Screenshot](/PythonicAPI/Utilities/Screenshot) |
[SelectExamples](/PythonicAPI/Utilities/SelectExamples) | Given a VTK Class and a language, select the matching examples.
[ShareCamera](/PythonicAPI/Utilities/ShareCamera) | Share a camera between multiple renderers.
[VTKWithNumpy](/PythonicAPI/Utilities/VTKWithNumpy) |
[XMLColorMapToLUT](/PythonicAPI/Utilities/XMLColorMapToLUT) | Take an XML description of a colormap and convert it to a VTK colormap.

### Arrays

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[ArrayCalculator](/PythonicAPI/Utilities/ArrayCalculator) | Perform in-place operations on arrays.
[ArrayToTable](/PythonicAPI/InfoVis/ArrayToTable) | Convert a vtkDenseArray to a vtkTable.
[ArrayWriter](/PythonicAPI/Utilities/ArrayWriter) | Write a DenseArray or SparseArray to a file.
[ExtractArrayComponent](/PythonicAPI/Utilities/ExtractArrayComponent) | Extract a component of an array.

### Events

## Math Operations

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[MatrixInverse](/PythonicAPI/Math/MatrixInverse) | Matrix inverse.
[MatrixTranspose](/PythonicAPI/Math/MatrixTranspose) | Matrix transpose.


## Graphs

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[AdjacencyMatrixToEdgeTable](/PythonicAPI/Graphs/AdjacencyMatrixToEdgeTable) | Convert an adjacency matrix to an edge table.
[AdjacentVertexIterator](/PythonicAPI/Graphs/AdjacentVertexIterator) | Get all vertices connected to a specified vertex.
[ConstructTree](/PythonicAPI/Graphs/ConstructTree) | Construct a tree.
[CreateTree](/PythonicAPI/Graphs/CreateTree) | Create a tree and label the vertices and edges.
[EdgeListIterator](/PythonicAPI/Graphs/EdgeListIterator) | Iterate over edges of a graph.
[GraphToPolyData](/PythonicAPI/Graphs/GraphToPolyData) | Convert a graph to a PolyData.
[InEdgeIterator](/PythonicAPI/Graphs/InEdgeIterator) | Iterate over edges incoming to a vertex.
[LabelVerticesAndEdges](/PythonicAPI/Graphs/LabelVerticesAndEdges) | Label vertices and edges.
[MutableGraphHelper](/PythonicAPI/InfoVis/MutableGraphHelper) | Create either a vtkMutableDirectedGraph or vtkMutableUndirectedGraph.
[OutEdgeIterator](/PythonicAPI/Graphs/OutEdgeIterator) | Iterate over edges outgoing from a vertex.
[RandomGraphSource](/PythonicAPI/Graphs/RandomGraphSource) | Create a random graph.
[SideBySideGraphs](/PythonicAPI/Graphs/SideBySideGraphs) | Display two graphs side by side.
[ScaleVertices](/PythonicAPI/Graphs/ScaleVertices) | Size/scale vertices based on a data array.
[SelectedVerticesAndEdges](/PythonicAPI/Graphs/SelectedVerticesAndEdges) | Get a list of selected vertices and edges.
[SelectedVerticesAndEdgesObserver](/PythonicAPI/Graphs/SelectedVerticesAndEdgesObserver) | Get a list of selected vertices and edges using an observer of AnnotationChangedEvent.
[VisualizeDirectedGraph](/PythonicAPI/Graphs/VisualizeDirectedGraph) | Visualize a directed graph.

### Graph Conversions

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[DirectedGraphToMutableDirectedGraph](/PythonicAPI/Graphs/DirectedGraphToMutableDirectedGraph) | vtkDirectedGraph to vtkMutableDirectedGraph.
[MutableDirectedGraphToDirectedGraph](/PythonicAPI/Graphs/MutableDirectedGraphToDirectedGraph) | vtkMutableDirectedGraph to vtkDirectedGraph.
[TreeToMutableDirectedGraph](/PythonicAPI/Graphs/TreeToMutableDirectedGraph) | vtkTree to vtkMutableDirectedGraph

## Data Structures

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[AttachAttributes](/PythonicAPI/PolyData/AttachAttributes) | Attach attributes to a VTK array.
[CellTreeLocator](/PythonicAPI/PolyData/CellTreeLocator) | Points on an object using vtkCellTreeLocator.

### Timing Demonstrations

### KD-Tree

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[DataStructureComparison](/PythonicAPI/DataStructures/DataStructureComparison) | Illustrates, side by side, the differences between several spatial data structures
[KDTree](/PythonicAPI/DataStructures/KDTree) |
[KDTreeAccessPoints](/PythonicAPI/DataStructures/KDTreeAccessPoints) | Access the points of a KDTree.

### Oriented Bounding Box (OBB) Tree

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[OBBTreeExtractCells](/PythonicAPI/DataStructures/OBBTreeExtractCells) | Intersect a line with an OBB Tree and display all intersected cells.

### Octree

### Modified BSP Tree

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[IncrementalOctreePointLocator](/PythonicAPI/DataStructures/IncrementalOctreePointLocator) | Insert points into an octree without rebuilding it.
[ModifiedBSPTreeExtractCells](/PythonicAPI/DataStructures/ModifiedBSPTreeExtractCells) | Intersect a line with a modified BSP Tree and display all intersected cells.

### HyperTreeGrid

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[HyperTreeGridSource](/PythonicAPI/HyperTreeGrid/HyperTreeGridSource) | Create a vtkHyperTreeGrid.

## VTK Concepts

## Rendering

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[ColoredSphere](/PythonicAPI/Rendering/ColoredSphere) | A simple sphere.
[GradientBackground](/PythonicAPI/Rendering/GradientBackground) | Demonstrates the background shading options.
[InterpolateCamera](/PythonicAPI/Rendering/InterpolateCamera) | Use vtkCameraInterpolator to generate a smooth interpolation between camera views.
[LayeredActors](/PythonicAPI/Rendering/LayeredActors) | Demonstrates the use of two linked renderers. The orientation of objects in the non active layer is linked to those in the active layer.
[MotionBlur](/PythonicAPI/Rendering/MotionBlur) | Example of motion blur.
[MultipleLayersAndWindows](/PythonicAPI/Rendering/MultipleLayersAndWindows) | Demonstrates the use of four renderers in two layers.
[OutlineGlowPass](/PythonicAPI/Rendering/OutlineGlowPass) | Demonstrates how to render a object in a scene with a glowing outline.
[PBR_Anisotropy](/PythonicAPI/Rendering/PBR_Anisotropy) | Render spheres with different anisotropy values.
[PBR_Clear_Coat](/PythonicAPI/Rendering/PBR_Clear_Coat) | Render a cube with custom texture mapping and a coat normal texture.
[PBR_Edge_Tint](/PythonicAPI/Rendering/PBR_Edge_Tint) | Render spheres with different edge colors using a skybox as image based lighting.
[PBR_HDR_Environment](/PythonicAPI/Rendering/PBR_HDR_Environment) | Renders spheres with different materials using a skybox as image based lighting.
[PBR_Mapping](/PythonicAPI/Rendering/PBR_Mapping) | Render a cube with custom texture mapping.
[PBR_Materials](/PythonicAPI/Rendering/PBR_Materials) | Renders spheres with different materials using a skybox as image based lighting.
[PBR_Materials_Coat](/PythonicAPI/Rendering/PBR_Materials_Coat) | Render spheres with different coat materials using a skybox as image based lighting.
[PBR_Skybox](/PythonicAPI/Rendering/PBR_Skybox) | Demonstrates physically based rendering, a skybox and image based lighting.
[PBR_Skybox_Texturing](/PythonicAPI/Rendering/PBR_Skybox_Texturing) | Demonstrates physically based rendering, a skybox, image based lighting and texturing.
[PBR_Skybox_Anisotropy](/PythonicAPI/Rendering/PBR_Skybox_Anisotropy) | Demonstrates physically based rendering, a skybox, image based lighting, and anisotropic texturing.
[StringToImageDemo](/PythonicAPI/Rendering/StringToImageDemo) | Demonstrates how to generate images from strings.
[StripFran](/PythonicAPI/Rendering/StripFran) | Triangle strip examples. (a) Structured triangle mesh consisting of 134 strips each of 390 triangles. (b) Unstructured triangle mesh consisting of 2227 strips of average length 3.94, longest strip 101 triangles. Images are generated by displaying every other triangle strip.
[TransformSphere](/PythonicAPI/Rendering/TransformSphere) | The addition of a transform filter to [ColoredSphere](/PythonicAPI/Rendering/ColoredSphere).
[TransparentBackground](/PythonicAPI/Rendering/TransparentBackground) | Demonstrates the use of two renderers. Notice that the second (and subsequent) renderers will have a transparent background.

## Lighting

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[Light](/PythonicAPI/Lighting/Light) | Add a directional light to a scene.
[ShadowsLightsDemo](/PythonicAPI/Visualization/ShadowsLightsDemo) | Show lights casting shadows.

## Shaders

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[MarbleShaderDemo](/PythonicAPI/Shaders/MarbleShaderDemo) | Explore the parameter space with the sliders.

## Texture Mapping

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[AnimateVectors](/PythonicAPI/Texture/AnimateVectors) | One frame from a vector field animation using texture maps.
[TextureCutQuadric](/PythonicAPI/Texture/TextureCutQuadric) | Cut a quadric with boolean textures.
[TextureCutSphere](/PythonicAPI/Texture/TextureCutSphere) | Examples of texture thresholding using a boolean combination of two planes to cut nested spheres.
[TexturePlane](/PythonicAPI/Texture/TexturePlane) | Example of texture mapping.
[TextureThreshold](/PythonicAPI/Texture/TextureThreshold) | Demonstrate the use of scalar thresholds to show values of flow density on three planes.
[TexturedSphere](/PythonicAPI/Texture/TexturedSphere) | Texture a sphere.


## Tutorial

If you are new to VTK then these tutorials will help to get you started.

## Video

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[OggTheora](/PythonicAPI/Video/OggTheora) | Write out an AVI file.

## Visualization

See [this tutorial](http://www.vtk.org/Wiki/VTK/Tutorials/3DDataTypes) for a brief explanation of the VTK terminology of mappers, actors, etc.

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[AnnotatedCubeActor](/PythonicAPI/Visualization/AnnotatedCubeActor) | Annotated cube.
[AssignCellColorsFromLUT](/PythonicAPI/Visualization/AssignCellColorsFromLUT) | Demonstrates how to assign colors to cells in a vtkPolyData structure using lookup tables.
[AxisActor](/PythonicAPI/Visualization/AxisActor) | Generate a single axis.
[BillboardTextActor3D](/PythonicAPI/Visualization/BillboardTextActor3D) | Label points with billboards.
[Blow](/PythonicAPI/Visualization/Blow) | Ten frames from a blow molding finite element analysis.
[CameraActor](/PythonicAPI/Visualization/CameraActor) | Visualize a camera (frustum) in a scene.
[CameraModel1](/PythonicAPI/Visualization/CameraModel1) | Illustrate camera movement around the focal point.
[CameraModel2](/PythonicAPI/Visualization/CameraModel2) | Illustrate camera movement centered at the camera position.
[CaptionActor2D](/PythonicAPI/Visualization/CaptionActor2D) | Draw a caption/bubble pointing to a particular point.
[ClipSphereCylinder](/PythonicAPI/VisualizationAlgorithms/ClipSphereCylinder) | A plane clipped with a sphere and an ellipse. The two transforms place each implicit function into the appropriate position. Two outputs are generated by the clipper.
[ColoredAnnotatedCube](/PythonicAPI/Visualization/ColoredAnnotatedCube) | How to color the individual faces of an annotated cube.
[CollisionDetection](/PythonicAPI/Visualization/CollisionDetection) | Collison between two spheres.
[CombineImportedActors](/PythonicAPI/PolyData/CombineImportedActors) | Combine actors from a scene into one actor.
[CornerAnnotation](/PythonicAPI/Visualization/CornerAnnotation) | Write text in the corners of a window.
[CorrectlyRenderTranslucentGeometry](/PythonicAPI/Visualization/CorrectlyRenderTranslucentGeometry) | Correctly Rendering Translucent Geometry.
[CreateBFont](/PythonicAPI/VisualizationAlgorithms/CreateBFont) | A scanned image clipped with a scalar value of 1/2 its maximum intensity produces a mixture of quadrilaterals and triangles.
[CubeAxesActor](/PythonicAPI/Visualization/CubeAxesActor) | Display three orthogonal axes with  with labels.
[CubeAxesActor2D](/PythonicAPI/Visualization/CubeAxesActor2D) | This example uses the vtkCubeAxesActor2D to show your scene with axes to indicate the spatial extent of your data.
[CurvaturesNormalsElevations](/PythonicAPI/Visualization/CurvaturesNormalsElevations) | Gaussian and Mean curvatures of a surface with arrows colored by elevation to display the normals.
[Cursor2D](/PythonicAPI/Visualization/Cursor2D) |
[Cursor3D](/PythonicAPI/Visualization/Cursor3D) |
[DataSetSurface](/PythonicAPI/VisualizationAlgorithms/DataSetSurface) | Cutting a hexahedron with a plane. The red line on the surface shows the cut.
[DepthSortPolyData](/PythonicAPI/Visualization/DepthSortPolyData) | Poly Data Depth Sorting.
[DisplacementPlot](/PythonicAPI/VisualizationAlgorithms/DisplacementPlot) | Show modal lines for a vibrating beam.
[DistanceToCamera](/PythonicAPI/Visualization/DistanceToCamera) |
[EdgePoints](/PythonicAPI/Visualization/EdgePoints) | Generate points along an edge.
[FastSplatter](/PythonicAPI/Visualization/FastSplatter) | Convolve a splat image with every point in an input image.
[FireFlow](/PythonicAPI/VisualizationAlgorithms/FireFlow) | Combine geometry from a VRML file and a solution from a vtkUnstructuredGrid file.
[FireFlowDemo](/PythonicAPI/VisualizationAlgorithms/FireFlowDemo) |  Combine geometry from a VRML file and a solution from a vtkUnstructuredGrid file (interactive).
[FlyingHeadSlice](/PythonicAPI/VisualizationAlgorithms/FlyingHeadSlice) | Flying edges used to generate contour lines.
[FroggieSurface](/PythonicAPI/Visualization/FroggieSurface) | Construct surfaces from a segmented frog dataset. Up to fifteen different surfaces may be extracted. You can turn on and off surfaces and control the camera position.
[FroggieView](/PythonicAPI/Visualization/FroggieView) | View surfaces of a segmented frog dataset using preprocessed `*.vtk` tissue files. You can turn on and off surfaces, control their opacity through the use of sliders and control the camera position.
[Hanoi](/PythonicAPI/Visualization/Hanoi) | Towers of Hanoi.
[HanoiInitial](/PythonicAPI/Visualization/HanoiInitial) | Towers of Hanoi - Initial configuration.
[HanoiIntermediate](/PythonicAPI/Visualization/HanoiIntermediate) | Towers of Hanoi - Intermediate configuration.
[HeadBone](/PythonicAPI/VisualizationAlgorithms/HeadBone) | Marching cubes surface of human bone.
[HyperStreamline](/PythonicAPI/VisualizationAlgorithms/HyperStreamline) | Example of hyperstreamlines, the four hyperstreamlines shown are integrated along the minor principal stress axis. A plane (colored with a different lookup table) is also shown.
[IsosurfaceSampling](/PythonicAPI/Visualization/IsosurfaceSampling) | Demonstrates how to create point data on an isosurface.
[Kitchen](/PythonicAPI/Visualization/Kitchen) | Demonstrates stream tracing in a kitchen.
[LODProp3D](/PythonicAPI/Visualization/LODProp3D) | Level of detail rendering.
[LabelPlacementMapper](/PythonicAPI/Visualization/LabelPlacementMapper) | Display a non-overlapping text label at each point.
[LabeledMesh](/PythonicAPI/Visualization/LabeledMesh) | Label Mesh.
[LoopShrink](/PythonicAPI/Visualization/LoopShrink) | A network with a loop. VTK 5.0 and later do not allow you to execute a looping visualization network; this was possible in previous versions of VTK.
[MovableAxes](/PythonicAPI/Visualization/MovableAxes) | Movable axes.
[Office](/PythonicAPI/VisualizationAlgorithms/Office) | Using random point seeds to create streamlines.
[OfficeA](/PythonicAPI/VisualizationAlgorithms/OfficeA) | Corresponds to Fig 9-47(a) in the VTK textbook.
[OfficeTube](/PythonicAPI/VisualizationAlgorithms/OfficeTube) | The stream polygon. Sweeping a polygon to form a tube.
[ProteinRibbons](/PythonicAPI/Visualization/ProteinRibbons) | Display Protein Data Bank ribbons.
[PineRootConnectivity](/PythonicAPI/VisualizationAlgorithms/PineRootConnectivity) | Applying the connectivity filter to remove noisy isosurfaces.
[PineRootConnectivityA](/PythonicAPI/VisualizationAlgorithms/PineRootConnectivityA) | The isosurface, with no connectivity filter applied.
[PineRootDecimation](/PythonicAPI/VisualizationAlgorithms/PineRootDecimation) | Applying the decimation and connectivity filters to remove noisy isosurfaces and reduce data size.
[PlateVibration](/PythonicAPI/VisualizationAlgorithms/PlateVibration) | Demonstrates the motion of a vibrating beam.
[PointDataSubdivision](/PythonicAPI/Visualization/PointDataSubdivision) | Demonstrates the use of the vtkLinearSubdivisionFilter and vtkButterflySubdivisionFilter.
[ProgrammableGlyphFilter](/PythonicAPI/Visualization/ProgrammableGlyphFilter) | Generate a custom glyph at each point.
[ProgrammableGlyphs](/PythonicAPI/Visualization/ProgrammableGlyphs) | Generate programmable glyphs.
[PseudoVolumeRendering](/PythonicAPI/VolumeRendering/PseudoVolumeRendering) | Here we use 20 cut planes, each with an opacity of of 0.25. They are then rendered back-to-front to simulate volume rendering.
[QuadricVisualization](/PythonicAPI/Visualization/QuadricVisualization) | Visualizing a quadric function.
[ReverseAccess](/PythonicAPI/Visualization/ReverseAccess) | Demonstrates how to access the source (e.g. vtkSphereSource) of an actor reversely.
[StreamlinesWithLineWidget](/PythonicAPI/VisualizationAlgorithms/StreamlinesWithLineWidget) | Using the vtkLineWidget to produce streamlines in the combustor dataset.  The StartInteractionEvent turns the visibility of the streamlines on; the InteractionEvent causes the streamlines to regenerate themselves.
[TensorEllipsoids](/PythonicAPI/VisualizationAlgorithms/TensorEllipsoids) | Display the scaled and oriented principal axes as tensor ellipsoids representing the stress tensor.
[TransformActorCollection](/PythonicAPI/Visualization/TransformActorCollection) | Transform an actor collection.
[VoxelsOnBoundary](/PythonicAPI/ImageData/VoxelsOnBoundary) | Extract voxels on the border of an isosurface.
[WarpCombustor](/PythonicAPI/VisualizationAlgorithms/WarpCombustor) | Carpet plots of combustor flow energy in a structured grid. Colors and plane displacement represent energy values.

## Working with ?vtkImageData?

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[ImageNormalize](/PythonicAPI/ImageData/ImageNormalize) | Normalize an image.
[WriteReadVtkImageData](/PythonicAPI/ImageData/WriteReadVtkImageData) | Generate, edit and read out vtk image data.


## Volume Rendering

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[IntermixedUnstructuredGrid](/PythonicAPI/VolumeRendering/IntermixedUnstructuredGrid) | A mix of poly data and unstructured grid volume mapping.
[MinIntensityRendering](/PythonicAPI/VolumeRendering/MinIntensityRendering) | Min intensity rendering.
[MultiBlockVolumeMapper](/PythonicAPI/VolumeRendering/MultiBlockVolumeMapper) | Using a vtkMultiBlockVolumeMapper to render a vtkMultiBlockDataSet containing eight subvolumes.
[RayCastIsosurface](/PythonicAPI/VolumeRendering/RayCastIsosurface) | Isosufaces produced by volume rendering.

## User Interaction

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[AreaPicking](/PythonicAPI/Picking/AreaPicking) | Area Picking.
[Assembly](/PythonicAPI/Interaction/Assembly) | Combine/group actors into an assembly.
[CallBack](/PythonicAPI/Interaction/CallBack) | Setting up a callback with client data. Two different methods are demonstrated.
[CellPicking](/PythonicAPI/Picking/CellPicking) | Cell Picking.
[EllipticalButton](/PythonicAPI/Interaction/EllipticalButton) | Create an elliptical button.
[HighlightPickedActor](/PythonicAPI/Picking/HighlightPickedActor) | Pick and highlight an actor based on mouse clicks.
[HighlightSelection](/PythonicAPI/Picking/HighlightSelection) | Highlight selection.
[HighlightWithSilhouette](/PythonicAPI/Picking/HighlightWithSilhouette) | Highlight a picked actor by adding a silhouette.
[ImageClip](/PythonicAPI/Interaction/ImageClip) | Demonstrates how to interactively select and display a region of an image.
[ImageRegion](/PythonicAPI/Interaction/ImageRegion) | Select a region of an image.
[InteractorStyleTrackballActor](/PythonicAPI/Interaction/InteractorStyleTrackballActor) |
[InteractorStyleTrackballCamera](/PythonicAPI/Interaction/InteractorStyleTrackballCamera) |
[MouseEvents](/PythonicAPI/Interaction/MouseEvents) | Subclass the interactor style.
[MouseEventsObserver](/PythonicAPI/Interaction/MouseEventsObserver) | Use an observer.
[RubberBand3D](/PythonicAPI/Interaction/RubberBand3D) |
[RubberBandZoom](/PythonicAPI/Interaction/RubberBandZoom) |
[StyleSwitch](/PythonicAPI/Interaction/StyleSwitch) | Choose between multiple interaction modes.



## Working with Images

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[BackgroundImage](/PythonicAPI/Images/BackgroundImage) | Display an image as the "background" of a scene, and render a superquadric in front of it.
[CannyEdgeDetector](/PythonicAPI/Images/CannyEdgeDetector) | Perform Canny edge detection on an image.
[DotProduct](/PythonicAPI/Images/DotProduct) | Compute the pixel-wise dot product of two vector images.
[ExtractComponents](/PythonicAPI/Images/ExtractComponents) | Extract components of an image. This can be used to get, for example, the red channel of an image.
[ImageCityBlockDistance](/PythonicAPI/Images/ImageCityBlockDistance) | Compute the Manhattan distance from every point to every black point in a binary image.
[ImageGridSource](/PythonicAPI/Images/ImageGridSource) | Create a image of a grid.
[ImageHistogram](/PythonicAPI/Images/ImageHistogram) | Compute the histogram of an image.
[ImageMask](/PythonicAPI/Images/ImageMask) | Mask a region of an image.
[ImageNonMaximumSuppression](/PythonicAPI/Images/ImageNonMaximumSuppression) | Find peaks in an image using non maximum suppression.
[ImageOpenClose3D](/PythonicAPI/Images/ImageOpenClose3D) | Open or close (morphologically) an image.
[ImageOrientation](/PythonicAPI/Images/ImageOrientation) | Reorder the axes of the image.
[ImageRange3D](/PythonicAPI/Images/ImageRange3D) | Replace every pixel with the range of its neighbors according to a kernel.
[ImageSeparableConvolution](/PythonicAPI/Images/ImageSeparableConvolution) | Convolve a separable kernel with an image.
[ImageSlice](/PythonicAPI/Images/ImageSlice) | Visualize and interact with an image. This is even more powerful than vtkImageSliceMapper. It can also do oblique slices.
[ImageStack](/PythonicAPI/Images/ImageStack) | Display layers of images.
[ImageToPolyDataFilter](/PythonicAPI/Images/ImageToPolyDataFilter) | Convert a vtkImageData to a vtkPolyData.
[MarkKeypoints](/PythonicAPI/Images/MarkKeypoints) | Mark keypoints in an image.


## Image Processing

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[Attenuation](/PythonicAPI/ImageProcessing/Attenuation) | This MRI image illustrates attenuation that can occur due to sensor position.  The artifact is removed by dividing by the attenuation profile determined manually.
[CenterAnImage](/PythonicAPI/Images/CenterAnImage) | Center an image.
[CombiningRGBChannels](/PythonicAPI/Images/CombiningRGBChannels) | Combine layers into an RGB image.
[EnhanceEdges](/PythonicAPI/ImageProcessing/EnhanceEdges) | High-pass filters can extract and enhance edges in an image. Subtraction of the Laplacian (middle) from the original image (left) results in edge enhancement or a sharpening operation (right).
[GaussianSmooth](/PythonicAPI/ImageProcessing/GaussianSmooth) | Low-pass filters can be implemented as convolution with a Gaussian kernel.
[HybridMedianComparison](/PythonicAPI/ImageProcessing/HybridMedianComparison) | Comparison of median and hybrid-median filters. The hybrid filter preserves corners and thin lines, better than the median filter.
[IdealHighPass](/PythonicAPI/ImageProcessing/IdealHighPass) | This figure shows two high-pass filters in the frequency domain. The Butterworth high-pass filter has a gradual attenuation that avoids ringing produced by the ideal high-pass filter with an abrupt transition.
[ImageContinuousDilate3D](/PythonicAPI/Images/ImageContinuousDilate3D) | Dilate an image.
[ImageContinuousErode3D](/PythonicAPI/Images/ImageContinuousErode3D) | Erode an image.
[ImageConvolve](/PythonicAPI/Images/ImageConvolve) | Convolve an image with a kernel.
[ImageCorrelation](/PythonicAPI/Images/ImageCorrelation) | Correlate two images.
[ImageDifference](/PythonicAPI/Images/ImageDifference) | Compute the difference image of two images.
[ImageDivergence](/PythonicAPI/Images/ImageDivergence) | Divergence of a vector field.
[ImageEllipsoidSource](/PythonicAPI/Images/ImageEllipsoidSource) | Create an image of an ellipsoid.
[ImageGradient](/PythonicAPI/VisualizationAlgorithms/ImageGradient) | Create an imaging pipeline to visualize gradient information.
[ImageGradientMagnitude](/PythonicAPI/Images/ImageGradientMagnitude) | Compute the magnitude of the gradient at each pixel of an image.
[ImagePermute](/PythonicAPI/Images/ImagePermute) | Reorder the axes of the image.
[ImageSobel2D](/PythonicAPI/Images/ImageSobel2D) | Sobel edge detection 2D.
[ImageVariance3D](/PythonicAPI/Images/ImageVariance3D) | Construct a new image consisting of the variance of the input image at each pixel.
[ImageWarp](/PythonicAPI/Images/ImageWarp) | Combine the imaging and visualization pipelines to deform an image in the z-direction. The vtkMergeFilter is used to combine the warped surface with the original color data.
 [IsoSubsample](/PythonicAPI/ImageProcessing/IsoSubsample) | This figure demonstrates aliasing that occurs when a high-frequency signal is subsampled. High frequencies appear as low frequency artifacts. The left image is an isosurface of a skull after subsampling. The right image used a low-pass filter before subsampling to reduce aliasing.
[MorphologyComparison](/PythonicAPI/ImageProcessing/MorphologyComparison) | This figure demonstrates various binary filters that can alter the shape of segmented regions.
[Pad](/PythonicAPI/ImageProcessing/Pad) | Convolution in frequency space treats the image as a periodic function. A large kernel can pick up features from both sides of the image. The left image has been padded with a constant to eliminate wraparound during convolution. On the right, mirror padding has been used to remove artificial edges introduced by borders.
[RGBToHSI](/PythonicAPI/Images/RGBToHSI) | Convert RGB to HSI.
[RGBToHSV](/PythonicAPI/Images/RGBToHSV) | Convert RGB to HSV.
[RGBToYIQ](/PythonicAPI/Images/RGBToYIQ) | Convert RGB to YIQ.
[ResizeImage](/PythonicAPI/Images/ResizeImage) | Resize an image using a sinc interpolator.
[VTKSpectrum](/PythonicAPI/ImageProcessing/VTKSpectrum) | The discrete Fourier transform changes an image from the spatial domain into the frequency domain, where each pixel represents a sinusoidal function. This figure shows an image and its power spectrum displayed using a logarithmic transfer function.

## Widgets

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[AffineWidget](/PythonicAPI/Widgets/AffineWidget) | Apply an affine transformation interactively.
[AngleWidget](/PythonicAPI/Widgets/AngleWidget) |
[AngleWidget2D](/PythonicAPI/Widgets/AngleWidget2D) | vtkAngleWidget + vtkAngleRepresentation2D.
[BalloonWidget](/PythonicAPI/Widgets/BalloonWidget) | Uses a vtkBalloonWidget to draw labels when the mouse stays above an actor.
[BiDimensionalWidget](/PythonicAPI/Widgets/BiDimensionalWidget) | Used to measure a 2D (X, Y) size of some aspect of an object.
[BorderWidget](/PythonicAPI/Widgets/BorderWidget) | 2D selection, 2D box.
[BoxWidget](/PythonicAPI/Widgets/BoxWidget) | This 3D widget defines a region of interest that is represented by an arbitrarily oriented hexahedron with interior face angles of 90 degrees (orthogonal faces). The object creates 7 handles that can be moused on and manipulated.
[BoxWidget2](/PythonicAPI/Widgets/BoxWidget2) |  This 3D widget defines a region of interest that is represented by an arbitrarily oriented hexahedron with interior face angles of 90 degrees (orthogonal faces). The object creates 7 handles that can be moused on and manipulated. ?vtkBoxWidget2? and ?vtkBoxRepresentation? are used in this example.
[CameraOrientationWidget](/PythonicAPI/Widgets/CameraOrientationWidget) | Demonstrates a 3D camera orientation widget.
[CaptionWidget](/PythonicAPI/Widgets/CaptionWidget) |
[CheckerboardWidget](/PythonicAPI/Widgets/CheckerboardWidget) | Compare two images using a checkerboard.
[CompassWidget](/PythonicAPI/Widgets/CompassWidget) | Draws an interactive compass.
[ContourWidget](/PythonicAPI/Widgets/ContourWidget) | Draw a contour (line) which can be deformed by the user.
[DistanceWidget](/PythonicAPI/Widgets/DistanceWidget) |
[HoverWidget](/PythonicAPI/Widgets/HoverWidget) | How to detect a hover.
[ImagePlaneWidget](/PythonicAPI/Widgets/ImagePlaneWidget) |
[ImageTracerWidgetInsideContour](/PythonicAPI/Widgets/ImageTracerWidgetInsideContour) | Highlight pixels inside a non-regular region scribbled on an image.
[ImageTracerWidgetNonPlanar](/PythonicAPI/Widgets/ImageTracerWidgetNonPlanar) | Draw on a non-planar surface.
[ImplicitAnnulusWidget](/PythonicAPI/Widgets/ImplicitAnnulusWidget) | Clip polydata with an implicit annulus.
[ImplicitConeWidget](/PythonicAPI/Widgets/ImplicitConeWidget) | An interactive implicit cone widget.
[ImplicitPlaneWidget2](/PythonicAPI/Widgets/ImplicitPlaneWidget2) | Clip polydata with an implicit plane.
[LineWidget2](/PythonicAPI/Widgets/LineWidget2) |
[LogoWidget](/PythonicAPI/Widgets/LogoWidget) | Logo widget.
[PlaneWidget](/PythonicAPI/Widgets/PlaneWidget) | Interact with a plane.
[PolygonalSurfacePointPlacer](/PythonicAPI/PolyData/PolygonalSurfacePointPlacer) | Used in conjunction with vtkContourWidget to draw curves on a surface.
[RectilinearWipeWidget](/PythonicAPI/Widgets/RectilinearWipeWidget) | Compare two images.
[Slider2D](/PythonicAPI/Widgets/Slider2D) | 2D Slider.
[Slider3D](/PythonicAPI/Widgets/Slider3D) | 3D Slider.
[SphereWidget](/PythonicAPI/Widgets/SphereWidget) | This 3D widget defines a sphere that can be interactively placed in a scene.
[SplineWidget](/PythonicAPI/Widgets/SplineWidget) | This example shows how to use vtkSplineWidget with a callback being used to get the length of the spline widget.

## Plotting

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[AreaPlot](/PythonicAPI/Plotting/AreaPlot) | Plot the area between two curves.
[BoxChart](/PythonicAPI/Plotting/BoxChart) | Box plot.
[ChartMatrix](/PythonicAPI/Plotting/ChartMatrix) | Create a marix of plots.
[FunctionalBagPlot](/PythonicAPI/Plotting/FunctionalBagPlot) | Functional Bag Plot.
[Histogram2D](/PythonicAPI/Plotting/Histogram2D) | 2D Histogram of a vtkImageData.
[HistogramBarChart](/PythonicAPI/Plotting/HistogramBarChart) | Histogram using bar chart.
[LinePlot2D](/PythonicAPI/Plotting/LinePlot2D) | Line plot of 2D data.
[LinePlot3D](/PythonicAPI/Plotting/LinePlot3D) | Line plot of 3D data.
[MultiplePlots](/PythonicAPI/Plotting/MultiplePlots) | Display multiple plots by using viewports in a single render window.
[ParallelCoordinates](/PythonicAPI/Plotting/ParallelCoordinates) | Parallel coordinates.
[PieChart](/PythonicAPI/Plotting/PieChart) | Pie chart.
[PieChartActor](/PythonicAPI/Plotting/PieChartActor) | Pie chart.
[ScatterPlot](/PythonicAPI/Plotting/ScatterPlot) | Scatter plot.
[SpiderPlot](/PythonicAPI/Plotting/SpiderPlot) | Spider plot.
[StackedBar](/PythonicAPI/Plotting/StackedBar) | Stacked bar.
[SurfacePlot](/PythonicAPI/Plotting/SurfacePlot) | Surface plot.

## Animation

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[AnimateActors](/PythonicAPI/Animation/AnimateActors) | Animate actors.
[AnimateSphere](/PythonicAPI/Animation/AnimateSphere) | Animate a sphere by opening it to an angle of 90°.
[Animation](/PythonicAPI/Utilities/Animation) | Move a sphere across a scene.
[AnimationScene](/PythonicAPI/Animation/AnimationScene) | Animation (the right way). Zoom in on a sphere.

## Annotation

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[LegendScaleActor](/PythonicAPI/Annotation/LegendScaleActor) | Display the scale of a scene.
[PolarAxesActor](/PythonicAPI/Annotation/PolarAxesActor) | Draw polar axes in a specified plane for a given pole.
[TextOrigin](/PythonicAPI/Annotation/TextOrigin) | This example demonstrates the use of vtkVectorText and vtkFollower. vtkVectorText is used to create 3D annotation.

## InfoVis

| Example Name | Description | Image |
| -------------- | ------------- | ------- |
[ParallelCoordinatesView](/PythonicAPI/InfoVis/ParallelCoordinatesView) | How to use Parallel Coordinates View to plot and compare data set attributes.

## PyQt
